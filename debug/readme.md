## 300x600 Banner debug

Úkolem je opravit vzhled reklamního banneru na stránce, kde chybějí některé povinné prvky a je rozbito formátování (viz. úkoly).

Žádné externí knihovny nesmí být použity. Mohou být použity pouze externí open source fonty (např. google fonts).

Kód musí být kompatibilní se staršími prohlížeči and nesmí rozbít styly a jiné prvky a styly na stránce.

Můžete psát kód mezi komentáře: `<!--    Edit only between these comments    -->`.

Úkoly:

- [ ] vycenterujte obsahu banneru jak **horizontálně**, tak i **vertikálně**
- [ ] opravte reklamní popisek "Reklama", tak aby byl viditelný v banneru
- [ ] zjistěte proč se banner nezobrazuje na mobilním zařízení.
- [ ] (volitelné) nastylujete banner tak, aby více zaujmul uživatele stránky.
- [ ] (volitelné) upravte HTML a CSS tak, aby se banner zobrazil i na mobilních zařízení.
