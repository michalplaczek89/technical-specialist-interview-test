
### Struktura URL

Rozdělte následující url na jednotlivé prvky a pojmenujte je.

```
https://test.r2b2.cz/a1/b2?article=12&widgets[]=a&widgets[]=b#foo
```

### Priority selektorů

Vysvětlete jakou prioritu má jaký typ selektoru v CSS. Uvedtě na Vámi vybraném příkladu.

### Kompatibilita s prohlížeči

Jak zjistíte zda je Vaše CSS kompatibilní s určitými prohlížeči?
